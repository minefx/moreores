package com.minefx.moreores.util;

public interface IHasModel
{
    public void registerModels();
}
