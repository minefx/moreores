package com.minefx.moreores.util;

public class Reference {
    public static final String MOD_ID = "mo";
    public static final String NAME = "MoreOres";
    public static final String VERSION = "0.1";
    public static final String ACCEPTED_VERSIONS = "{1.12.2}";
    public static final String CLIENT_PROXY_CLASS = "com.minefx.moreores.proxy.ClientProxy";
    public static final String COMMON_PROXY_CLASS = "com.minefx.moreores.proxy.CommonProxy";


}
