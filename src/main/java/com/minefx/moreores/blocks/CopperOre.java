package com.minefx.moreores.blocks;

import java.util.Random;

import com.minefx.moreores.init.ModItems;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;

public class CopperOre extends BlockBase{
	public CopperOre(String name, Material material) {
		super(name, material);
		setSoundType(SoundType.METAL);
		setHardness(2.0F);
		setResistance(2.0F);
		setHarvestLevel("pickaxe", 1);
	}

}
