package com.minefx.moreores.init;

import java.util.ArrayList;
import java.util.List;

import com.minefx.moreores.blocks.BlockBase;
import com.minefx.moreores.blocks.CopperOre;
import com.minefx.moreores.blocks.RubyOre;
import com.minefx.moreores.blocks.SapphireOre;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class ModBlocks
{
	public static final List<Block> BLOCKS = new ArrayList<Block>();
	
	public static final Block RUBY_BLOCK = new BlockBase("ruby_block", Material.IRON);
	public static final Block RUBY_ORE = new RubyOre("ruby_ore", Material.ROCK);
	public static final Block COPPER_ORE = new CopperOre("copper_ore", Material.ROCK);
	public static final Block COPPER_BLOCK = new BlockBase("copper_block", Material.IRON);
	public static final Block SAPPHIRE_BLOCK = new BlockBase("sapphire_block", Material.IRON);
	public static final Block SAPPHIRE_ORE = new SapphireOre("sapphire_ore", Material.ROCK);
}
